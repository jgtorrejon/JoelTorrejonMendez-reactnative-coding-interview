import { StyleSheet, Platform } from 'react-native';

export default StyleSheet.create({
  employeeItem: {
    flexDirection: 'row',
    padding: 10,
    paddingVertical: 15,
    ...Platform.select({
      android: {
        borderRadius: 10,
        borderColor: '#000',
        borderWidth: 0.5,
        marginBottom: 0.5,
      },
    }),
  },
  employeeAvatar: {
    height: 48,
    width: 48,
    borderWidth: 1,
    borderRadius: 100,
    marginHorizontal: 10,
  },
  employeeInfo: {
    justifyContent: 'space-around',
  },
});
